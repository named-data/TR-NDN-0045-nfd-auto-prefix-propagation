\documentclass[conference]{sig-alternate-10pt}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% figure-related settings
\usepackage{graphicx}
\graphicspath{{fig/}, {exp/}}
\DeclareGraphicsExtensions{.pdf,.jpeg,.png,.eps,.jpg}
\usepackage[tight,footnotesize]{subfigure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% math-algorithm settings
\usepackage{amsmath, amssymb, algorithmic}
\usepackage[lined, boxed, comments numbered, ruled, linesnumbered]{algorithm2e}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% other settings
\usepackage{multirow, longtable, url}
\usepackage{footmisc} % define footref
\usepackage[svgnames, table]{xcolor} % for color
\usepackage{xifthen}% provides \isempty test
\usepackage{setspace}
\usepackage{authblk}% for multiple affiliations of one author 
\usepackage{cite}
\usepackage[font=scriptsize,margin=.5cm]{caption}
\usepackage{color,soul}
\usepackage{xspace}
%\usepackage{calc}
\newcommand{\TODO}[1]{\hl{\textbf{TODO:} #1}\xspace}
\newcommand{\todo}[1]{\TODO{#1}}
\newcommand{\paper}{report~}

\newcommand{\NOTE}[1]{\textcolor{red}{\textbf{NOTE: #1}} \newline}
\newcommand{\FIXME}[1]{\textcolor{red}{\textbf{{FIXME: #1}}} \newline}
\newcommand{\fixme}[1]{\FIXME{#1}}

\newfont{\nicettfont}{cmtt9}
%\newcommand{\ndnName}[1]{``{\nicettfont #1}''}
\newcommand{\schema}[1]{``{\nicettfont #1}''}

\def\UrlBreaks{\do-}
\def\cmd#1{\textit{#1}}
\def\sample#1{\textit{#1}}

%% \name command
%% - allows break before /
\usepackage{etoolbox}
\usepackage{xstring}
\DeclareListParser{\doslashlist}{/}
\newcounter{ndnNameComponentCounter}%
\newcommand{\ndnName}[1]{{%
		\setcounter{ndnNameComponentCounter}{0}%
		\renewcommand{\do}[1]{{%
				\ifnumgreater{\value{ndnNameComponentCounter}}{0}{\allowbreak/}{}%
				\ifnumodd{\value{ndnNameComponentCounter}}{}{}%
				\detokenize{##1}}%
			\stepcounter{ndnNameComponentCounter}}%
		``{\fontfamily{cmtt}\small\selectfont\IfBeginWith{#1}{/}{/}{}\doslashlist{#1}}''%
}}

\def\name#1{\textbf{#1}}
\newcommand{\method}[3][]{%
{\small
\setstretch{0.2}
  \begin{flalign*}
  \ifthenelse{\isempty{#1}}%
    {}% if #1 is empty
    {& \textcolor{DarkGreen}{\mathtt{#1}} &&\\}% if #1 is not empty
     & \begin{aligned}
       \textcolor{DarkBlue}{\fontfamily{pcr}\selectfont
         \textbf{#2}}\textbf{(}#3\textbf{)}
     \end{aligned}&&
  \end{flalign*}
}
}
\newcommand{\argu}[2]{%
  &\textcolor{DarkGreen}{\mathtt{#1}}~#2
}

\newcommand{\nfdconf}[2]{%
{\small
\setstretch{0.2}
  \begin{flalign*}
    &\textcolor{DarkBlue}{\fontfamily{pcr}\selectfont \textbf{#1}}&&\\
    &\begin{aligned} #2 \end{aligned}&&
  \end{flalign*}
}
}

\newcommand{\nfdbra}[2]{%
   &\begin{aligned}
   &\mathtt{#1}\\
   &\{\\
   &~~\begin{aligned}
        #2
      \end{aligned}\\
   &\}
   \end{aligned}\\
}
\newcommand{\nfdv}[2]{%
  &\begin{aligned}
  &\mathtt{#1}~\mathit{#2}\\
  \end{aligned}\\
}
\newcommand{\nfdc}[1]{%
{\small
   $$\mathtt{#1}$$
}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{authblk}

\title{NDN Automatic Prefix Propagation}

\author[1]{Yanbiao Li}
\author[2]{Alexander Afanasyev}
\author[3]{Junxiao Shi}
\author[1]{Haitao Zhang}
\author[1]{Zhiyi Zhang}
\author[1]{Tianxiang Li}
\author[1]{Edward Lu}
\author[3]{Beichuan Zhang}
\author[4]{Lan Wang}
\author[1]{Lixia Zhang}

\affil[1]{University of California, Los Angeles\\
  \protect\url{[lybmath,haitao,zhiyi,tianxiang,edward,lixia]@cs.ucla.edu}}
\affil[2]{Florida International University\\
  \protect\url{aa@cs.fiu.edu}}
\affil[3]{The University of Arizona\\
  \protect\url{[shijunxiao,bzhang]@email.arizona.edu}}
\affil[4]{The University of Memphis\\
  \protect\url{lanwang@memphis.edu}}


\date{}

\usepackage{eso-pic,xcolor}
\makeatletter
\AddToShipoutPicture*{%
\setlength{\@tempdimb}{20pt}%
\setlength{\@tempdimc}{\paperheight}%
\setlength{\unitlength}{1pt}%
\put(\strip@pt\@tempdimb,\strip@pt\@tempdimc){%
    \makebox(0,-60)[l]{\color{blue}%
NDN, Technical Report NDN-0045. \url{http://named-data.net/techreports.html}}
  }%
\put(\strip@pt\@tempdimb,\strip@pt\@tempdimc){%
    \makebox(0,-85)[l]{\color{black}%
Revision 1: March 18, 2018}
  }%
}
\makeatother

\begin{document}
\maketitle

%\section*{Revision history}
%\begin{itemize}
%\item \textbf{Revision 1 (September 23, 2015)}:
%  Initial revision
%\item \textbf{Revision 2 (June 19, 2019)}:
%  Reshape or rewrite some sections
%\end{itemize}
%\TODO{need to state this is revision 2, with a date?}

\begin{abstract}
In an NDN network, when a producer application wants to publish data, it registers the data's name prefix $P$ with the local NDN forwarding Daemon (NFD) on the same host machine;
this registration informs the local NFD where to forward Interests whose name falls under $P$.
To propagate the reachability to $P$ beyond the local NFD requires additional mechanisms, such as running routing protocol.
%To provide similar option to the remote NDN forwarders, they need to get configured manually, use dynamic routing protocols, or rely on opportunistic data discovery.
This paper describes \emph{automatic prefix propagation} protocol, an alternative to running a full-featured routing protocol on host machines connected to the NDN network via one or multiple NDN gateway routers.
The automatic prefix propagation protocol uses the local configuration of NDN certificates and the inferred presence of gateway router(s) to automatically send remote registration request when necessary.

\end{abstract}

%\TODO{Do not use contractions in academic paper: don't=>do not, doesn't =>does not, etc.}
%\TODO{AA: As you may saw in intro.  TR doesn't describe a brand-new work, so avoid ``propose'', ``new'', etc.  Just describe what it does.  we use, prefix propagation uses, etc.}

\input{introduction}

\input{motivation}

\input{design}

\input{flow}

\input{conclusion}

\section*{Acknowledgment}
%\addcontentsline{toc}{section}{Acknowledgment}

This work is partially supported by the National Science Foundation under awards CNS-1629922, and CNS-1719403.

\bibliographystyle{abbrv}
\bibliography{reference}

%\appendix
%\input{function}
%\input{instruction}

\end{document}
