\section{Function manual}

Here, we review all functions of the \name{AutoPrefixPropagator}
module in three sections: 1) public interfaces, 2) helpers and 3)
functions related to the propagated-entry state
machine.

\subsection{Public Interfaces}
\label{sect:imp:interface}

\subsubsection{Constructor}
\label{sect:imp:constructor}

\method{AutoPrefixPropagator}{%
  \argu{ndn::nfd::Controller\&}{controller},\\
  \argu{ndn::KeyChain\&}{keyChain},\\
  \argu{Rib\&}{rib}
}

When creating an instance of \name{AutoPrefixPropagator}, three arguments are required: 1) a reference to the NFD controller that was used to send out the commands for propagation / revocation, 2) a reference to the key-chain owned by the NFD that supplies identities, and 3) a reference to the managed RIB that maintains two important signals, \sample{Rib::afterInsertEntry} and \sample{Rib::afterEraseEntry}, which will trigger propagations and revocations respectively.

\subsubsection{loadConfig}
\label{sect:imp:loadConfig}

\method[void]{loadConfig}{\argu{const~ConfigSection\&}{configSection}}

This method is invoked when the \name{RibManager} is loading configurations from a specified NFD config file~\cite{NFD-ConfigFile} at the \sample{rib} section. All propagation-related parameters are configured at the \sample{rib.auto\_prefix\_propagate} subsection, which is loaded, parsed, and then passed to this method.

\subsubsection{enable}
\label{sect:imp:enable}

\method[void]{enable}{}

This method is invoked by the \name{RibManager} to enable the automatic prefix propagation feature.

To achieve this, the method \sample{afterInsertRibEntry}\footnote{All methods of \name{AutoPrefixPropagator} are referred to without specifying the namespace.} is connected to the signal \sample{Rib::afterInsertEntry}, which is emitted after an RIB entry has been inserted. Likewise, another method \sample{afterEraseRibEntry} is connected to \sample{Rib::afterEraseEntry}, which is emitted after an existing RIB entry has been erased.

\subsubsection{disable}
\label{sect:imp:disable}

\method[void]{disable}{}

In contrast with \sample{enable}, these two signal connections are released to disable the feature of automatic prefix propagation.

\subsection{Propagation Helpers}
\label{sect:imp:helper}

\subsubsection{getPrefixPropagationParameters}
\label{sect:imp:getPrefixPropagationParameters}

\method[PrefixPropagationParameters]{getPrefixPropagationParameters}{%
  \argu{const~Name\&}{localRibPrefix}
}

The return value, \name{PrefixPropagationParameters}, is a structure that can be used by the registration commands for prefix propagations / revocations. It consists of an instance of \name{ControlParameters} (\sample{parameters}) and an instance of \name{CommandOptions} (\sample{options}), as well as a bool variable (\sample{isValid}) which indicates whether this set of parameters is valid (i.e., the signing identity exists).

This method is invoked to get the required parameters for prefix propagation. Given a local RIB prefix \sample{localRibPrefix}, it finds (in the local key-chain) a proper identity whose namespace covers the input prefix. If there is no desired identity, it returns an invalid \name{PrefixPropagationParameters}. Otherwise, it sets the selected identity as the signing identity of \sample{options}. Additionally, it sets this identity as the name of \sample{parameters}.

\subsubsection{doesCurrentPropagatedPrefixWork}
\label{sect:imp:doesCurrentPropagatedPrefixWork}

\method[bool]{doesCurrentPropagatedPrefixWork}{%
  \argu{const~Name\&}{prefix}
}

This method is invoked before re-performing some propagation to check whether a currently propagated prefix still works. A propagated prefix still works if and only if the identity corresponding to this prefix still exists in the key-chain, and there is no shorter identity that covers this prefix. Otherwise, either current signing identity can not be found, or a better choice exists and should be adopted.

\subsubsection{redoPropagation}
\label{sect:imp:redoPropagation}

\method[void]{redoPropagation}{%
  \argu{PropagatedEntryIt}{entryIt},\\
  \argu{const~ControlParameters\&}{parameters},\\
  \argu{const~CommandOptions\&}{options},\\
  \argu{time::seconds}{retryWaitTime}
}

This method is invoked to refresh a successful propagation, to retry a failed propagation, or to awake a suspended propagation. For all these uses, the propagation for the input propagated entry is performed with the input \name{ControlParameters}, \name{CommandOptions} and current setting of waiting period before retry, as long as its propagated prefix still works (determined by \sample{doesCurrentPropagatedPrefixWork}). Otherwise, the input propagated entry will be erased, while all local RIB entries represented by it will be re-handled to set up required propagations.

\subsubsection{advertise}
\label{sect:imp:advertise}

\method[void]{advertise}{%
  \argu{const~ControlParameters\&}{parameters},\\
  \argu{const~CommandOptions\&}{options},\\
  \argu{time::seconds}{retryWaitTime}
}

This method is invoked to send out the registration command for a propagation with given input parameters. Two callbacks, \sample{afterPropagateSucceed} and \sample{afterPropagateFail}, are assigned for the cases when the propagation succeeds or fails.

Before sending out the command, two events are created (but not scheduled at this point) and passed as arguments to those two callbacks: \sample{afterPropagateSucceed} to refresh, and \sample{afterPropagateFail} to retry.

 The retry event requires an argument to define the waiting period before next retry, which is calculated
 according to the back-off policy based on the current waiting period (\sample{retryWaitTime}) and \sample{m\_maxRetryWait}.

\subsubsection{withdraw}
\label{sect:imp:withdraw}

\method[void]{withdraw}{%
  \argu{const~ControlParameters\&}{parameters},\\
  \argu{const~CommandOptions\&}{options},\\
  \argu{time::seconds}{retryWaitTime}
}

This method is invoked to send out the unregistration command for revoking a propagation with given input parameters. Two callbacks, \sample{afterRevokeSucceed} and \sample{afterRevokeFail}, are assigned for the case when the revocation succeeds or fails.

\subsubsection{afterInsertRibEntry}
\label{sect:imp:afterInsertRibEntry}

\method[void]{afterInsertRibEntry}{%
  \argu{const~Name\&}{prefix}
}

This method is invoked once the signal \sample{Rib::afterInsertEntry} is emitted. As discussed in section~\ref{sect:design:when}, not all rib entry insertions will necessarily lead to propagations. The process is abandoned if the local RIB entry whose insertion triggers this invoking is scoped for local use only (i.e., the name of that RIB entry,\sample{prefix}, starts with \sample{/localhost}). On the other hand, if \sample{prefix} is just the \name{link local nfd prefix}, i.e., this invoking is triggered by establishing a connectivity to the router, \sample{afterHubConnect} is invoked to awake all suspended propagations.

Even though \sample{prefix} represents an RIB entry that can trigger propagation, the triggered propagation may be unnecessary. This is the case if there is no valid \name{PrefixPropagationParameters} found for this entry, or the propagation has already been processed.

If there is a necessary propagation triggered, then \sample{afterRibInsert} is invoked to process this request further.

\subsubsection{afterEraseRibEntry}
\label{sect:imp:afterEraseRibEntry}

\method[void]{afterEraseRibEntry}{%
  \argu{const~Name\&}{prefix}
}

Like \sample{afterInsertRibEntry}, this method is invoked once the signal \sample{Rib::afterEraseEntry} is emitted. If \sample{prefix}, the name of the local RIB entry whose deletion triggered this invoking, is scoped for local use only, this invoking terminates immediately. If this invoking was triggered by the deletion of the \name{link local nfd prefix}, i.e., the connectivity to the router was lost, \sample{afterHubDisconnect} is then invoked to suspend all propagations by cancelling their scheduled retry / refresh events. In all other cases, a revocation will be required.

However, if there is no valid \name{PrefixPropagationParameters} found, the required revocation will not be performed. If the propagation to be revoked has not succeeded yet, or the corresponding propagated entry should be kept for other RIB entries, the required revocation will also be abandoned.

Lastly, if a revocation is truly necessary, \sample{afterRibErase} is then invoked.

\subsection{State-machine related functions}

\subsubsection{afterRibInsert}
\label{sect:imp:afterRibInsert}

\method[void]{afterRibInsert}{%
  \argu{const~ControlParameters\&}{parameters},\\
  \argu{const~CommandOptions\&}{options}
}

Once this event occurs, a new propagated entry will be created, i.e., from \sample{RELEASED} to \sample{NEW}. If there is an active connectivity to the router, \sample{advertise} is invoked to perform this propagation, and the state of the created entry is further switched to \sample{PROPAGATING}. Otherwise, this propagation will be suspended. The retry waiting period is set to \sample{m\_baseRetryWait}.

\subsubsection{afterRibErase}
\label{sect:imp:afterRibErase}

\method[void]{afterRibErase}{%
  \argu{const~ControlParameters\&}{parameters},\\
  \argu{const~CommandOptions\&}{options}
}

Once this event occurs, the propagated entry will be erased, i.e., switched to \sample{RELEASED}. If there is an active connectivity and the corresponding propagated entry has succeeded, \sample{withdraw} is invoked to revoke this propagation. Since the \name{Cost} field of \sample{parameters} is set by default, it should be unset to adapt to the unregistration command.

\subsubsection{afterHubConnect}
\label{sect:imp:afterHubConnect}

\method[void]{afterHubConnect}{}

When this event occurs, all suspended propagations are awakened by invoking \sample{redoPropagation} for each of them.

\subsubsection{afterHubDisconnect}
\label{sect:imp:afterHubDisconnect}

\method[void]{afterHubDisconnect}{}

When this event occurs, all propagations are suspended by initializing their corresponding propagated entries.

\subsubsection{afterPropagateSucceed}
\label{sect:imp:afterPropagateSucceed}

\method[void]{afterPropagateSucceed}{%
  \argu{const~ControlParameters\&}{parameters},\\
  \argu{const~CommandOptions\&}{options},\\
  \argu{const~Scheduler::Event\&}{refreshEvent}
}

When this event occurs, the propagated entry may be in the \sample{RELEASED} state (in addition to the \sample{PROPAGATING} state), because this entry may be erased due to revocation before the result of this propagation gets back.

If the propagated entry does not exist (i.e., in the \sample{RELEASED} state), an unregistration command is sent immediately to revoke this propagation. A copy of \sample{parameters} is made and passed to \sample{withdraw} with the \name{Cost} field unset.

If the state of the propagated entry is \sample{PROPAGATING}, it should be switched to \sample{PROPAGATED}, and the refresh event \sample{refreshEvent} is scheduled to redo this propagation after a duration (\sample{m\_refreshInterval}).

\subsubsection{afterPropagateFail}
\label{sect:imp:afterPropagateFail}

\method[void]{afterPropagateFail}{%
  \argu{uint32\_t}{code},\\
  \argu{const~std::string\&}{reason}, \\
  \argu{const~ControlParameters\&}{parameters},\\
  \argu{const~CommandOptions\&}{options},\\
  \argu{time::seconds}{retryWaitTime},\\
  \argu{const~Scheduler::Event\&}{retryEvent}
}

When this event occurs, the propagated entry may be in the \sample{RELEASED} state (in addition to the \sample{PROPAGATING} state), because the same prefix may be propagated by another local RIB entry before the result of this revocation gets back.

 If the propagated entry still exists, its state is switched to \sample{PROPAGATE\_FAIL}, and the retry event \sample{retryEvent} is scheduled to redo this propagation after a duration defined by the current waiting period for retry, \sample{retryWaitTime}.

\subsubsection{afterRevokeSucceed}
\label{sect:imp:afterRevokeSucceed}

\method[void]{afterRevokeSucceed}{%
  \argu{const~ControlParameters\&}{parameters},\\
  \argu{const~CommandOptions\&}{options},\\
  \argu{time::seconds}{retryWaitTime},\\
}

When this event occurs, besides the \sample{RELEASED} state, the propagated entry may also be in any other states except the \sample{NEW} state. This is because the same prefix may be propagated by another local RIB entry before the result of this revocation gets back (\sample{PROPAGATING}), and the result of that propagation may be back earlier (\sample{PROPAGATED} or \sample{PROPAGATE\_FAIL}).

If the propagated entry is in the \sample{PROPAGATING} state or the \sample{PROPAGATED} state, \sample{advertise} will be invoked, since the propagation on the router has been revoked but it should be kept in this case. As \sample{parameters} is used for revocation (i.e., the \name{Cost} filed is not set), a copy of it is made and passed to \sample{advertise} with the \name{Cost} set as that of \sample{m\_controlParameters}.

\subsubsection{afterRevokeFail}
\label{sect:imp:afterRevokeFail}

\method[void]{afterRevokeFail}{%
  \argu{uint32\_t}{code},\\
  \argu{const~std::string\&}{reason}, \\
  \argu{const~ControlParameters\&}{parameters},\\
  \argu{const~CommandOptions\&}{options}
}

When this event occurs, the propagated entry stays in whatever state it is in, and nothing need to be done.

\subsubsection{onRefreshTimer}
\label{sect:imp:onRefreshTimer}

\method[void]{onRefreshTimer}{%
  \argu{const~ControlParameters\&}{parameters},\\
  \argu{const~CommandOptions\&}{options}
}

When this event occurs, the propagated entry must be in the \sample{PROPAGATED} state. The \sample{redoPropagation} is invoked to handle this refresh request.

\subsubsection{onRetryTimer}
\label{sect:imp:onRetryTimer}

\method[void]{onRetryTimer}{%
  \argu{const~ControlParameters\&}{parameters},\\
  \argu{const~CommandOptions\&}{options},\\
  \argu{time::seconds}{retryWaitTime},\\
}

When this event occurs, the propagated entry must be in the \sample{PROPAGATE\_FAIL} state. The \sample{redoPropagation} is invoked to handle this retry request.